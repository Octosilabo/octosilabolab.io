---
layout: page
title: "El porqué"
permalink: /about/
image: images/ vasco_raul_y_yo.jpg
comments: true 
---

![#piloto]({{ site.baseurl }}/images/vasco_raul_y_yo.jpg)


En los tiempos de la universidad, en tiempos de dictadura, junto a dos compañeros, Raúl y Vasco, todos pobres como las ratas, para pasarlo bien los días de invierno, salíamos a los pueblos a hacer versos en cuartetas. 

Lo hacíamos con los mismos trucos de los que hacen lucha libre, todo de mentira. Es decir contrapuntos subidos de tono, que se suponían improvisados, pero eran de memoria. La gente reía, nos aplaudía, nos convidaban vino, empanadas y las chicas del lugar nos preferían para a bailar guaracha. 

A los tres nos hubiera encantado tener los recursos de aprendizaje a la mano, para dominar el arte del verso improvisado y poder expresarnos de verdad. 

Pero no había cómo. Menos tan lejos de la zona central, de donde son los cultores originales de Chile. 

Nos quedamos así, sin aprender a versear.

Raúl murió en noviembre del 2012 en Antofagasta y Vasco en marzo del 2017 en Rancagua. 

 La idea de este podcast es fomentar al verso popular, a sus cultores, a sus tradiciones y a los modos en que éstos transmiten sus saberes.