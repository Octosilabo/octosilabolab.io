---
layout: post  
title: "GABRIEL PAREDES. El montubio Universal"  
date: 2020-04-20  
categories: podcast  
image: images/GABRIEL_PAREDES.jpg  
podcast_link: https://ia801408.us.archive.org/0/items/gabriel-paredes/GABRIEL_PAREDES.mp3
tags: [audio, poesia, octosilabo]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/GABRIEL_PAREDES.jpg)

<audio controls>
    <source src="https://ia801408.us.archive.org/0/items/gabriel-paredes/GABRIEL_PAREDES.mp3" type="audio/mpeg">
</audio>

**GABRIEL PAREDES. El montubio Universal**

Entrevistamos a Gabriel dentro de lo que fue la bóveda de un banco, en la Biblioteca de la Universidad de las Artes en Guayaquil. Nos cuenta de su arte, de su cultura de su pueblo.


Sigue octosílabo.com en:

+ Twitter: <https://twitter.com/octosilabos>
+ Correo: <unicornioazul@disroot.com>
+ Web: <https://octosilabo.gitlab.io/>
+ Youtube: <https://www.youtube.com/channel/UCcfzvytq_DgGQBJOA8v-TNw>
+ Feed Podcast: <https://octosilabo.gitlab.io/feed>
