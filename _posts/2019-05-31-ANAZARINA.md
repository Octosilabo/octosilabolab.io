---
layout: post  
title: "Las Rimas de Ana Zarina y su libro Jugando con la Rima"  
date: 2019-05-31  
categories: podcast  
image: images/ANA_ZARINA_YOUTUBE.png  
podcast_link: https://ia601509.us.archive.org/18/items/ANAZARINAPALAFOX/ANAZARINAPALAFOX.mp3
tags: [audio, poesia, octosilabo]  
comments: true 
---
![#piloto]({{ site.baseurl }}/images/ANA_ZARINA_YOUTUBE.png)

<audio controls>
    <source src="https://ia601509.us.archive.org/18/items/ANAZARINAPALAFOX/ANAZARINAPALAFOX.mp3" type="audio/mpeg">
</audio>

**Las Rimas de Ana Zarina y su libro Jugando con la Rima**


****octosilabo.com:**** ¿Cómo fue de que tú tuviste una conexión con la poesía y con el verso en particular?

**Ana Zarina Palafox:** Mira de muy niña sin saber lo que estaba pasando, ya escuchaba yo versos cantados en las canciones. Muchos años después me enteré que el autor que tanto escuché de los discos de mi padre, Edmundo Rivero, era un poeta. Y cuando no cantaba poesías de él, seleccionaba con mucho cuidado las poesías exactamente del mismo tipo que las usábamos en las formas tradicionales de música, no solo en México sino en otros países de Iberoamérica. Con estrofas isométricas con décimas, con cuartetas etc. Con cierta formalidad pues en la estrofa. De niña sin querer absolutamente sin querer.

**octosilabo.com:** Lo aprendiste como aprendiste el castellano, como aprendiste a comer, como aprendiste a lavarte los dientes.

**Ana Zarina Palafox:** Sí vaya yo llegué a una casa donde ya estaba eso aunque no había esta cultura analítica al respecto pues. Mis papás oían los discos, punto. Los degustaban, los seleccionaban, pero no hablaban ellos particularmente del mensaje lírico, Y, a pesar de que a mí me llamó mucho la atención. No sabía que se llamaba así, no. No sabía que era una cosa. . . un oficio por sí mismo. Me dices que, que no hay un momento que yo detecte. . . Es que hay varios, desde llegar a una casa donde el verso ya estaba cantado. Otro momento cuando yo empecé a tocar ya profesionalmente, donde quería cambiarle la letra a las canciones siempre. Porque yo tenía que decir cosas, yo quería decir cosas, a veces de broma a veces porque quería adaptar un mensaje en serio. Pero ahí estaba cambiándole la letra, decía yo, improvisando versos ahora sé que se llama, no. Y otro momento fue cuando yo llego a un lugar muy tradicional en cuanto al verso y a la música en México que se llama Tlacotalpan Veracruz y ahí fue donde yo me topo con el oficio. Te estoy hablando de 1986.

**octosilabo.com:** Era una estructura mayor en tu vida

**Ana Zarina Palafox:** Era una estructura mayor, era un oficio en sí mismo el verso, independientemente de los compositores, cantautores etc. Pero en ese momento me enteré que si se valía decir los versos, si se valía hacer los versos, valía cambiarlos, hacer variantes y era una cosa hasta deseable. Ese fue el padrinazgo digamos de Tlacotalpan para mí.

**octosilabo.com:** Te acuerdas tú, como fue tu golpe, así. El momento mismo en que tu dijiste ¡GUAU! ¡Esto es! ¿Fue un momento o fue un proceso ahí de descubrimiento lento? ¿Cómo fue?

**Ana Zarina Palafox:** Específicamente estaba oyendo decimistas de los decidores pues, de poesía propia o ajena, pero los que memorizan y la dicen. Pero se acercó a mí un personaje que se llama Marcos Gómez Cruz de apodo el “Taconazo”, en Tlacotalpan. Él tenía jaranas en su casa y arpas, en ese momento pues para los amigos que llegaran me vio ahí en la plaza, este, le dije que tocaba el arpa, yo estaba buscando músicos hicimos plática, nos invitó a su casa los compañeros que andaban conmigo y a  mí, sacó los instrumentos y cuando estábamos ya tocando ya entendiéndonos tocando, empezó a hacerme versos y fue así el guau, y empecé yo a intentarlo torpemente, pero pero los lograba, los cuadraba en la música y él me los contestaba y fue así como llegué al nirvana pues, llegué a donde. . . llegué a la tierra prometida vaya.

**octosilabo.com:** Ya ese fue, ese fue tu encuentro, pero después me imagino que tuviste de... no sé si habrá sido largo o corto, pero de aprender las técnicas. Como en cualquier arte ¿No?. Uno parte por lo formal para después desarrollar tu potencial ¿No? 

**Ana Zarina Palafox:** No con este hombre Marcos Gómez Cruz, él no me guio nada, él me hizo versos y yo se los contesté, entonces fue completamente empírico. Es totalmente contrario a cómo estás diciendo de la parte formal. Yo me enteré que había parte formal ya que estaba improvisando bastante hábilmente, porque no es este. . . En México por lo menos hasta los noventas, no era una cosa habitual que se enseñara el verso que alguien te guiara para absolutamente nada. Estaba, este, la idea que a la gente se le da el verso o no se le da así como por iluminación divina, generación espontánea una cosa así, entonces bueno, se me dio, no diría eso sino yo creo que fue mi estimulación temprana la que me llevó a poderlo hacer con bastante facilidad cuando ya me encontré con que se podía y fue mi propio análisis porque incluso con la misma décima aun cuando fue para escribirla, que no para improvisarla al principio. Yo no abrí un diccionario de versificación, este un libro de literatura, nada, este. Me enteré que se llamaba décima, por que los mismos decimistas dicen: en décima te lo digo, en décima te lo canto, décima, décima. Te enteras a fuerza. ¿No? Ahora sí que es auto definitoria la décima, pero me enteré eso. No porque, no porque alguien me dijera que era una forma estrófica de diez líneas octosilábicas, nada. Lo que hice fue que escuchaba yo las décimas, transcribía algunas y vi cómo iba la rima. Punto. En ese momento yo creí que lo estaba deduciendo a partir de la nada, a partir de las décimas que estaba oyendo. Años después me enteré que, psss, que yo ya oía décimas desde bebé, pues claro que me fue muy fácil ¿No? Diría María Montessori: Primero interioricé la cosa y ya después la explicité ¿No? Ese fue el proceso conmigo.

**octosilabo.com:** Porque cuando uno habla de décimas a veces, este la gente te habla... los que saben ¿No? Te dicen: mira una décima es una redondilla... dos redondillas... una quitilla... dos quintillas juntas. Y empiezan, digamos con eso... con, con, con... la estructura y que es la primera con la cuarta y la quinta, la segunda con la tercera, la sexta con la séptima y así... O sea, y empieza un enredo en la cabeza y uno se empieza totalmente a complicar con esa, esos números de versos ¿No? Ahora, tu cuando escuchaste o cuando dedujiste esto, no dijiste ¡Uf! ¡Que cansancio esto! Que ¡Que agobio! 

**Ana Zarina Palafox:** No, no, no porque yo ya estaba totalmente atrapada, estaba en una jaula a lo mejor yo creo que no jaulas, sino barca a la libertad por la expresión pero bueno, yo ya estaba allí completamente atrapada con la sonoridad resonándome en la cabeza, tú ya estas así como si están bailando mucho si tu llegas y por lo menos la cabeza mueves al compás de los que están bailando, así se me ha ido moviendo la palabra a mí y bueno, ya hacerlo explícito na´ más fue como pa´ entender. Pero ya estaba atrapada por la sonoridad.

**octosilabo.com:** O sea te hizo sentido, fue, no, no las palabras en si. Ni su rima, ni la estructura de las estrofas ni nada de esas cosas. Sino que la cadencia, el ritmo, la melodía si se quiere 

**Ana Zarina Palafox:** Digamos que la palabra me ha fascinado desde que la conocí, desde que todavía no podía hablarla ya me fascinaba la palabra, la palabra por sí misma. El asunto de poder compartir abstracciones entre dos seres, vamos. Sea una paleta de limón o sea un atardecer glorioso, pero el hecho de poder compartirle a alguien que no tiene eso que tú tienes y regalárselo aunque sea en imagen, ya me fascinaba. Me fascinaba que me leyeran cuentos, etc. Pero la cosa que me refiero con la sonoridad, te lo voy a poner como ejemplo con una estrofa mexicana que hasta tú conoces. Si yo llego y te digo: oye voy a narrar la historia de un hombre que le gustaba la canción pero además le gustaba hacerla en horas muy tempranas en el día y es una persona más bien tendiente a la monarquía o por lo menos de raigambre monárquica y es una tipo además que esta cuestión estética discriminatoria, porque solamente a las mujer con ciertas cualidades estéticas, les puede decir, hacer patentes sus dones. Eso si te da flojera, pero si te digo: estas son las mañanitas que cantaba el Rey David a las mujeres bonitas se las cantamos aquí. ¡Ya! ¿Para qué te tiro tanto rollo, no?

**octosilabo.com:** Adivina cual es el pie forzado. Estas son las mañanitas

**Ana Zarina Palafox:** 

Gracias por el pie forzado

y pornombrarme poeta

eso no da mucha veta

lo siento medio encumbrado

pero en fin estoy al lado

de unas pantallas bonitas

con ese pie tu me quitas

la magia de este momento

mas canto con sentimiento

estas son las mañanitas.

Lo quise hacer así lo que Alexis dice improvisación pura.

**octosilabo.com:** Siempre el artista de la improvisación está restringido a decir lo que quiere decir. Dice lo que puede decir, en el momento y en la circunstancia y con las condiciones que se le dan. ¿Es así o no?

**Ana Zarina Palafox:** Es así. Y hay momentos más afortunados y otros menos afortunados. ¿No?

Es cuestión de la cultura,

Es cuestión de la cultura

y es un tema muy extenso,

pero ahora que lo pienso 

la poesía no está madura 

cuando la noche es oscura 

son las estrella bonitas

y una idea necesitas 

que te sirve como un broche 

si la poesía es la noche 

estas son las mañanitas.

**octosilabo.com:** Yo creo de que tú vas a seguir con ese pie forzado.

**Ana Zarina Palafox:** Mira, te la quise hacer porque ese es un ejemplo un poco más pensada, ya que me has dado el pie hace rato y ahí ya fue una armada un poquito mas complejo ¿No?

**octosilabo.com:** Pero finalmente tu nunca tuviste como tal un mentor, alguien ella o él.¿Que tu consideres que fue quién te guio por este camino? ¿Tu eres una self made woman? 

**Ana Zarina Palafox:** Nadie es un self made,nadie, nada. Este, creo que ni los gatos son self made.Este, te podría nombrar así como influencias importantísimas, así ultra importantes Edmundo Rivero el multi citado, con décima pero también con soneto y con sixtilla armandiana, que fue lo primerito que yo conocí. Te voy a nombrar a Alfredo Zitarrosa como un muy principal, a Larralde y a Cafrune de Argentina, pero también te voy a nombrar a dos ya tres, que a uno ya te lo nombré en una plática a Tito Fernández “El Temucano”, pero también te voy a nombrar a Violeta Parra multi citada ¿No? Como maestra, aunque ella no me haya visto nunca y te voy a nombrar a Juan Capra también, que son quienes me anclaron, en si, en pensar, bueno la música está bonita y gusta tocarla y todo. Pero no es lo mismo aunque sea Atahualpa Yupanqui. Un los Ejes de mi Carreta, es más bien poesía quejumbrosa le podría yo decir ¿No? A escuchar cantores que reflexionan. Esos no son canciones, esos son libros de texto de filosofía avanzada y eso fue lo que a mí me llamó. Sobre todo los poetas sudamericanos. Me acerco en Tlacotalpan a la décima específicamente, pero también a la sextilla, quintillas dentro del Son Jarocho con este hombre Marcos Gómez Cruz que eso es lo que improvisaba ¿No?: décima. Y me acerco yla empiezo a practicar y todo el asunto. Pero, y siempre fui como la rara, por los temas o por la forma de abordarlos o por lo que sea. Yo no hablaba como los jarochos aunque estaba en zona jarocha y con jarochos interviniendo con ellos ¿No? Entonces señalar las varias etapas. Pero de todas formas era yo la rara, la distinta, Lautaro Merzari, es el argentino que estaba con nosotros y resulta que mi improvisación se parecía más a la de él ¿No? Y luego, también, pero también anduve entre huastecos muchos años y puedo tocar el huasteco pero no se parecía mi poesía a la de ellos. Algo tenía de extraño, aunque yo quisiera usar modismos ¿No? Pero luego, también estuve con la gente de tierra caliente, con la gente de la sierra gorda y no se acababa de parecer. Entonces, bueno, te lo digo en una cuarteta que hice al modo de la sierra gorda 

Yo soy una poeta chilanga

De palabras firmes y seguras 

pero no lo saco de la manga 

hago mezcla de muchas culturas 

**octosilabo.com:** ¿Cuáles son tus culturas Ana Zarina? ¿Dónde estás tus raíces?

**Ana Zarina Palafox:** Mis raíces.

Perdona que lo disponga

En estos versos felices

Preguntas por mis raíces 

y una es la milonga

Chile que Dios te lo ponga

con la paya a lo divino

ya lo dije, lo argentino

y también lo mexicano

eso llevo de la mano

por ahí va mi camino

En primera yo si me creí y me creo hasta la fecha, ese cuento de la Latinoamérica unida y el sueño bolivariano ¿No?

**octosilabo.com:** La Patria Grande

**Ana Zarina Palafox:** Claro

No puedo ser otra cosa

Mi querido Benjamín,

Traigo guitarrón, violín,

También la guitarra hermosa,

hoy me ilumina la glosa,

hoy me ilumina la cueca,

yo vengo de la huasteca 

y vengo del son jarocho 

este, mi corazón mocho

nunca la ha traído chueca.

**octosilabo.com:** Este fondo tuyo intelectual, emocional. ¿Cómo, porqué después lo pones en el escenario? ¿Por qué quieres hacerlo saber? ¿Por qué tú tienes la necesidad de expresarlo, que sea conocido lo que tu sientes a los demás?

**Ana Zarina Palafox:** Mira a los siete años empiezo a aprender guitarra, a los siete años ya estoy sentada en la mesa de hasta en frente de una peña folklórica, escuchando a la Violeta y a Atahualpa y a todos ellos en vivo antes de oírlos en grabación ¿No? Bueno, no a ellos pues, pero la música de ellos. Interpretada por cantores en vivo y a los siete años y medio o siete años tres cuartos estaba un cantor que se hacía llamar Juan Cruz, argentino, me enteré hace dos años que ese era su nombre artístico de un nuevo lanzamiento. Él se llamaba realmente Tati Vitalli y él iba a cantar una zamba y preguntó si alguien tocaba el bombo y otra mujer músico que estaba allí en la peña, que sabía que yo estaba en las clasecitas y ya me había oído tocar me dijo: súbete, súbete, súbete, y me subió y toqué el bombo. Tengo la grabación, mi madre estaba grabando para fortuna mía y al Tati Vitalli diciendo como habrán visto me acompaña una niña muy jovencita, mexicana que toca muy bien la zamba. Yo ya estaba en el escenario.

**octosilabo.com:** ¿Por qué escogiste dentro de todas la formas de expresión, artística esa forma de expresión que es la cultura tradicional como le llamas tú? ¿Cuál fue el imán que te llevó ahí?

**Ana Zarina Palafox:** No sé, no tengo idea. Mira son sensaciones, o sea, de los discos que había en la casa y que escuchaban mis papás y yo oía, y que después yo ponía cuando me enseñaron a usar la torna mesa. Había música del mundo, había canciones tirolesas, había canciones italianas, había música clásica, había zarzuela, toda la música la disfruté muchísimo, pero aquella milonga porteña, los tangos en lunfardo, y los quería cantar y me los aprendí y los canté ¿No? No sé por qué. Porque eso, eso lo sentía más orgánicamente mío. Y después, no quiero que vayas a pensar que lo estoy diciendo por porque tú naciste en Chile, pero es que llegó un amigo de mis papás que había ido a estudiar un post grado en actuaría a Chile y eran las primeras grabadoras portátiles de casetes y llegó con una grabadora a regalarle a mi madre y llegó con un casete que traía una cancioncita que empezaba con unas quenas muy simples y un coro así al unísono “Ya son demasiados que lo pasan mal...” Era un casete con la música de una carrera presidencia de Allende en Chile. Y traía también no sé porque la zamba de las tolderías: “Tristeza que se levanta el toldo de las tradiciones, del toldo traigo esta zamba. . .” y entonces sentí el cosquilleo en los dedos. O sea, yo nada más quería cantar.

**octosilabo.com:** Ahora nos vas a tener que contar de tu método de jugar con la rima. Hacer un método, para que alguien aprenda metódicamente, a hacer algo que tú hiciste intuitivamente.

**Ana Zarina Palafox:** Porque me gusta enseñar lo que sé y me gusta que me enseñen lo que saben los otros, ya, creo que es un bien comunitario que debería estar prohibido no enseñar, pues. Y entonces, bueno, empecé a dar los talleres, empecé a ver que iba atinando a cosas que funcionaban. Por ejemplo: no andar contando sílabas, sino manejar la rítmica ¿No? A lo mejor eso se me hizo fácil porque como soy músico, digo si yo, si yo te pongo a palmear: taf, taf, taf, taf, taf, taf, taf, taf, taf, taf, taf, taf, y te lo hago media hora, taf, taf, no te tengo que explicar si es cuatro cuartos o dónde está el puntillo, dónde entra el anacrusa, que la clave, la usan en las... ¡Nada! Te hago así y al rato tú estás taf, taf, palmeando conmigo. Pues así con la palabra ¿No? Los versos, a ver. . . las estrofas no son sino unas cajitas, o desde las matemáticas las puedo yo pensar, son matrices de tanto por tanto, o son unas áreas planas de tanto por tanto, o son tantas columnas por tantos reglones y simplemente se trata de ajustar el mensaje ahí, bueno, ¿Cuál mensaje? El que se le dé la gana a cada quién. Esa parte, esa parte, es lo que no se puede enseñar, porque tu expresividad es un cúmulo de tus vivencias, de tu estado de ánimo, de tus maledicencias, no sé de tus bendiciones, todo tú eres lo que quieres expresar. ¿Qué es lo que se puede enseñar del verso? Pos, del tamaño de la cajita y cómo ajustar las palabras ahí. 

**octosilabo.com:** Da la impresión de que es una cosa mágica ¿Ah? Tú te metes cuatro horas, ocho horas, o dos jornadas y sales haciendo versos octosílabos,
sextillas, quintillas, décimas, endecasílabos, o sea ¿Qué te puedo decir?

**Ana Zarina Palafox:** Que suena increíble.

**octosilabo.com:** No que suena increíble. ¡Es increíble!

**Ana Zarina Palafox:** Mira, a lo mejor con mí... con el formato que ha tenido mi vida y este estar ya donde estaban todos los estímulos alrededor para facilitármelo, todo esto se me ha hecho muy fácil, pues. O sea, yo te puedo decir que, que si le dedico mucho tiempo, que le dedico mucho tiempo a mejorar, a practicar etcétera, pero eso no quiere decir que sea difícil. A lo mejor laborioso, pero no difícil. Esta convicción de que no es difícil es la que yo quiero contagiar. Y, bueno, desde escuelas alternativas, desde pedagogos, este, que estaban involucrados en estas escuelas raras, dónde yo estuve y donde yo cursé la educación primaria, media, media superior, me hace estar consciente de los procesos de aprendizaje, estar consciente que esto no sea enseñanza sino aprendizaje y más o menos tener unas lucecitas como dicen de los psicólogos, un psicólogo esta igual de loco que tú, pero por lo menos tiene un mapa y una lámpara ¿No? Bueno, tenerla, tener un mapa y tener una lámpara para decirte, ver no llevarte por mi camino, eso es imposible, pero decirte: mira ahí donde hay una barda ni te metas. ¿Cómo la alcancé a ver yo? Pss tengo una lámpara ¿No? No, mira no vamos a virar a la izquierda porque tengo un mapa y me dice que la calle está cerrada de ese lado. Y ayudarte a irte llevando, a irte guiando con tu propio caminar, por esa facilidad que yo estoy convencida que existe. Ese es el punto.

**octosilabo.com:** Jugando con la Rima me dice de que en jornadas  de dos o tres días las personas logran algo que es realmente impresionante: aprender a hacer versos con estructuras y con rimas. ¿Este método, por qué es tan exitoso en tan corto tiempo?     

**Ana Zarina Palafox:** Estamos partiendo de que la masa humana considera que es muy difícil y esas es la idea hegemónica ¿Estamos? Y de que si alguien viene y te dice esto así no le crees por que la masa hegemónica te ha repetido mil veces que es difícil. Este método está organizado uno) y esto cualquier pedagogo va a decirte que es perfectamente adecuado, en pasos muy pequeños graduales, un paso a la vez ¿Si? Yo no te voy a enseñar a sumar si no conoces los números y ya luego que puedas sumar ya después te puedo enseñar a multiplicar ¿Si? ¡Bueno! Es mucho más fácil hacer versos con rítmica que aprender a sumar y multiplicar. Como involucramos, este, en las formas normales de enseñarlo tienden a explicártelo todo antes de dejarte hacerlo y te estoy hablando y tanto de un verso, como de un pilotaje de aviones a propulsión.... a ver...  Te voy a meter en un cuarto, te voy a poner en frente de una pizarra seiscientas horas donde te voy a poner unos dibujos de todos los medidores que hay en una cabina del avión, en el tablero, para el piloto y donde están todas las palanquitas y te pongo gráficas y te digo mira si esta palanca atrás y te lleno la cabeza de teorías ya que está llena y te lo aprendes de memoria. Entonces te hago un examen para que me demuestres que eso que te aprendiste de memoria no sabes eso es la forma más difícil. Si yo te subo luego al avión no quiero que te mates, pero al simulador de vuelo y en el momento en que te subes te digo: a ver uno por uno, para poner la trompa del avión hacia arriba tu vas a mover esta palanca hacia el frente, muévela kuickkk y vas a ver en una pantallita simulado como subiste. Ahora por favor baja, muévela al lado contrario kuickkk a lo mejor te estalla el simulador porque la bajaste muy rápido Pss silo estás haciendo en ese momento no tiene que pasar dos noches memorizando cual era la palanca para subir ya lo hiciste. Y en eso, te digo: segundo paso, a ver, hala la palanca muy suavemente hacia arriba, mientras ves un marcador en el tablero y vas a ver un avioncito como el horizonte va más hacia abajo o más hacia arriba del avioncito. Relaciona la palanca con lo que estás viendo en ese marcador. Es táctil, lo tienes en la mano, lo tienes en los ojos y te estoy diciendo el significado, todo al mismo tiempo. Y además te vas a emocionar y ese es un componente absolutamente indispensable, vas a sentir el hormigueo en el estómago y vas a sentir ¡Estoy subiendo! ¡Estoy subiendo! Ya te emocionaste. Eso es el catalizador para que tu cerebro haga sus conexiones y para que entre tus neuronas y tus    aparezcan    Que aparezcan esos foquitos que parecen fuegos artificiales en las resonancias magnéticas y tu cerebro está construyendo la zona especial que va a manejaresta información. Y que además va a combinar al mismo tiempo, date cuenta, un  movimiento del brazo, la suavidad del movimiento del brazo con lo visual del horizonte. Cuanto te subas al avión de a deveras, tu brazo ya está entrenado. Ese es el método, por eso parece mágico, no necesitamos llamar. . .saber que se llama sinalefa o hiato, gerundio, infinitivo. No necesitamos nada de eso. Necesitamos saber cómo suena para saber si rima o no con la otra. 

**octosilabo.com:** ¿Y eso es todo?

**Ana Zarina Palafox:** Y necesitamos saber cómo suena para saber si encaja o no con una rítmica fija.

**octosilabo.com:** Es como esos cursos de inglés que uno toma, no es cierto, en donde lo último que te enseñan es gramática. ¡Usted venga! Le voy a enseñar a hablar primero y después a leer y a escribir.

**Ana Zarina Palafox:** Te voy a demostrar con tu persona, que tengo razón, sí. 

**octosilabo.com:** ¿Con mi persona? No, si yo no te discuto, yo creo que tienes la razón 

**Ana Zarina Palafox:** No pero a tus oyentes les voy a demostrar con tu persona esto. 

**octosilabo.com:** No quiero, no quiero quedar mal, no quiero quedar mal. Pero bueno, me someto, me someto.

**Ana Zarina Palafox:** Contéstame un par de preguntas nada más, no te voy a hacer trovar. Dime ¿A qué edad aprendiste a hablar?

**octosilabo.com:** ¡Uf! ¿No sé? ¿Cuatro, cinco, cuatro? ¿Por ahí? Creo, no sé, je je je. No recuerdo, dije mamá, no sé algo así.

**Ana Zarina Palafox:**¿Y cuándo, y cuando? ¿Aprendiste a escribir? 

**octosilabo.com:** A los seis.

**Ana Zarina Palafox:** Entonces aprendiste a hablar primero y a escribir después ¿Verdad? Será el caso de todos los que nos están oyendo ¿Verdad?

**octosilabo.com:** Probablemente

**Ana Zarina Palafox:** Ok poniendo las cosas así no te parece absurdo, que la poesía se enseñe primero por escrito, cuando no es simplemente sino una forma de hablar ¿Más elaborada? ¿No te parece que los que están locos son todos ustedes? 

**octosilabo.com:** Estamos locos, es vedad. Ahora te voy a llevar a tu libro. Solamente te voy a hacer dos preguntas con respecto a tu libro. Hay algo que me llamó la atención que son las cinco leyes de oro del método, de Jugar con la Rima. ¿No? Voy a enunciarlas: La una es hablar fuerte y claro, la otra es poner atención para entender y recordar lo que otros dijeron. Tercero, pensar antes lo que voy a decir en mi turno. Cuarto: convivir, no competir. Y quinto: usar con responsabilidad el poder de la palabra acordándonos que el hecho de nombrar las cosas las hace existir. Estos principios que tu haces acá, son principios que. . .  tengo la percepción de que son más que simple y sencillamente principios metodológicos para poder aprender a hacer versos. Casi yo diría que son principios éticos. Explícanos que, que nos estás pasando de contrabando aquí. ¿A ver?          

**Ana Zarina Palafox:** Mira como los filósofos griegos tenían un trívium y un cuatrívium de las ciencias humanas, estas reglas de oro tienen un trívium y un vídium ¿No? Las tres primeras es algo que cualquier versador, payador, trovador, poeta o como se autonombre, quién sea que improvise en versos isométricos el mundo, este. . . ejercita. Y esa es una cosa, ahí si completamente empírica que me di cuenta conmigo misma y luego para facilitarle al otro, digamos que me van a contratar o no, a mi no me importa decirle que yo elegida y que no van a aprender nunca y que mejor me contraten a mi etssss comparto. A lo mejor me contratan para compartir. Todos los que ejercitan el verso improvisado hablan fuerte y claro, sino o no nos oyen o si hablan ellos nos alcanzan a oír pero lo hacen tímidamente, no me creen si te digo vengo de un planeta oscuro con destinoEl universo      No me haces caso, eso. Un hecho. Entonces hay que hablar fuerte y claro,punto. Poner atención. Oye, yo estoy payando con otro y es un diálogo de sordos, si ahorita no te pongo atención a lo que me preguntas. ¿Qué te contesto? Para el verso es básico, si estamos en una controversia, diálogo o como le queramos decir, topada, lo que sea. Tengo que estarte poniendo atención. Y tengo que pensar antes lo que tengo que decir en mi turno, como hace rato con la décima del pie forzado  “estas son las mañanitas”. Tú me estás diciendo el pie y yo ya estoy viendo cómo, cómo largo para decírtela de inmediato. Pero es una cuestión simultánea. Esas tres leyes son cosas que ejerces al mismo tiempo cuando estás en el escenario, o en una cantina o en tu recámara trovando con tu pareja no sé, con lo que sea. Las tres las ejerces y las ejerces al mismo tiempo. Y las otras dos, si son profundamente éticas y deseables. ¿Por qué? Porque son cosas que yo he visto en los payadores que más he admirado, vivos o muertos, y que se les transparenta simplemente hasta con la cuestión criminal de leer su paya y no escuchárselas en vivo ¿No? Porque sabes que de los versos improvisados, como tu bien dijiste hace rato, uno improvisa lo que puede en el momento, a veces puede algo más bonito a veces no. Cuando transcribes eso y lo dejas así: momificado en el papel. Pss puede ser tan cruel como un crucifijo, puedes estar viendo un cadáver ahí y no poesía. Pero de los que más admiro: Convivir no competir. Así hagamos una controversia, una masacre del tipo que hacen Yeray Rodríguez y Alexis Díaz-Pimienta, así hagamos eso, Yeray y Alexis no se golpean cuando bajan del escenario, se abrazan ¿N Se quieren y muchísimo vaya y por muchos años. Los Berstsolaris vascos me dijeron unacosa que hicieron explícita allá. Los Berstsolaris vascos tienen por regla, así estén en el campeonato de Bertsolaris con su público monumental en frente, la labor de uno cuando está trovando con el otro es facilitarle las cosas al otro para que brille y si el otro hace los mismo contigo, eso son unos fuegos artificiales preciosos. Cuando tú y yo nos estamos peleando como en esa película mexicana de Pedro el malo y Jorge el bueno, nosotros quedamos mal porque los dos hacemos quedar mal al otro ¿No? Y puede ser muy chistoso, pero eso éticamente no te deja nada. Y la última tiene una profundidad bastante fuerte ¿No? Porque tiene  muchas lecturas, tiene la lectura literal: “Usar con responsabilidad el poder de la palabra”. Si yo llego a una oficina de gobierno, algo que no nos gusta a nadie, porque voy a hacer un trámite y lo primero que digo es: “Oiga usted vieja, este, floja. Deje esa revista y atiéndame porque vengo enojado” ¡Olvídate de tu trámite para siempre! 

**octosilabo.com:** Eso es universal. El poder ejercido desde el púlpito je je je

**Ana Zarina Palafox:** Usas con responsabilidad el poder de la palabra desde si vas a hacer un trámite molesto llegas y le dices: “Buenos días señorita. ¿Cómo está usted?” Aunque no te importe la señora, ya estás usando con responsabilidad el poder de la palabra y a lo mejor voltea y te sonríe y ya te cae bien y ya ves más allá. Segunda etapa, si estás en un escenario o al frente de un salón de clases o en la calle, como los artistas callejeros, ejerciendo la palabra, date cuenta que vas a dejarle huella a quién te está oyendo por que el verso llama la atención por si mismo, porque porque es musical y entonces, bueno, cuida lo que dices. Y es tan válido decir, no sé: “Tan bonita margarita, tan bonita como tú” y si quieres hacer eso y transmitir esa belleza estética pura al otro, usas eso. Pero también es válido decir: “Que vivan los estudiantes, jardín de nuestra alegría”, pero también es válido decir: “Te recuerdo Amanda”. Son palabras poderosas y que en cuanto las emites algo le estás causando al otro y las palabras son como puñales. Entonces ¿Si quieres usarlas para matar? ¡Hazlo! Pero estate seguro que lo piensas hacer ¿No? 

**octosilabo.com:** Bueno, avancemos. Hay dos desafíos que tu propones en tu libro, que llevan a otro nivel el Método de Jugar con la Rima. Porque hasta Jugar con la Rima, tratar de hacer estructuras octosílabas, en cuatro versos, sean cuartetas o redondillas es algo que es abarcable en ocho horas o en dos días de intensa jornada de aprendizaje. Pero tu propones no solamente eso, tu propones en tu libro y en tu método de que alguien pueda aprender estructuras, estrofas de cinco versos, de seis versos, de diez versos. Y no conforme con eso, no solamente octosílabos, sino que hexasílabos, endecasílabos, alejandrinos. O sea, es que es una multitud de formas enorme. Sácame de una duda. Qué es más difícil de aprender, o de automatizar como dices tú en el método, mentalmente. ¿La cantidad de versos que tiene que llevar tu estrofa, la cantidad de sílabas que tiene que llevar tu verso? 

**Ana Zarina Palafox:** Ninguna es más difícil. Ninguna es difícil de hecho. A ver. La forma en la que yo los llevo, insisto es multi sensorial, con la cuestión rítmica, con el metro interno, les decimos los músicos, puedes deducir el ritmo. Te pongo un ejemplo: Dime una métrica, la que quieras. 

**octosilabo.com:** El octosílabo, que es la que me parece...

**Ana Zarina Palafox:** No tiene chiste, esa la conoce cualquiera.

**octosilabo.com:** Ese lo conoce cualquiera, je, je, je. Entonces el alejandrino, que me parece...

**Ana Zarina Palafox:** Alejandrino, catorce sílabas.

**octosilabo.com:** Catorce sílabas. Si.

**Ana Zarina Palafox:** Divide y vencerás catorce son siete y siete y ya me dijiste un octosílabo: “catorce son siete y siete” bueno, este: catorce son siete y siete todavía divido más, factorizo ¿No? Siete son tres y cuatro tacata tacataca, tacata tacataca, otro de esos tacatá tacataca, hemistiquio dicen los poetas cultos.

Te digo en cuatro versos la pregunta impecable

te digo de repente hoy frente a la pantalla

que todos asimilan lo difícil no sea haya

te digo en este verso lo saco de la mente

Divide y vencerás.

Alguna vez en un encuentro de formadores en Colombia,  el Primer encuentro de formadores en verso improvisado les di la demostración del taller. Ellos son mis compañeros, a parte de que eran puros hombres, este, y pensaron que a mi me habían mandado porque andaba con alguien, no sé. Este, ellos, cuando vieron el Método por escrito, o cuando se los empecé a explicar, igual no me creían nada ¿No? Y entonces como era un. . . como todos ellos son improvisadores y todos ellos auto silábicos y yo sé que el endecasílabo espanta hasta los poetas de escritorio, porque está muy raro en español, porque es una forma estrófica más bien del acento italiano. La onomatopeya que yo uso: tacataca tacataca es un octosílabo, o que usa Alexis: la la lero lero la la en ves de usar ese, por que ya no iban a sentir el rigor de ser novatos en algo, en vez de eso les hice un tacataca, luego tataca, luego les puse a hacer tacataca tataca, ya luego completo tacataca tataca tacataca,

te lo digo en tiempo, y hasta en forma 

te lo digo rimando tan tranquila

y tu oreja lo escucha lo asimila

y lo siente porque esta si es la horma

¡Ya!

Ahí estoy improvisando en endecasílabo, ello se dieron cuenta, que lo estaban haciendo ellos. Se les pararon los pelos, se asombraron, huelga decirte que en la noche en el convidio, la botella de ron en medio, disminuyendo alegremente. No trovaron en endecasílabo, pero el pie forzado que se agarraron dos horas

**octosilabo.com:** Ya.

**Ana Zarina Palafox:** Fue la teoría del tacataca.

**octosilabo.com:** je, je, je

**Ana Zarina Palafox:** Salimos abrazados y borrachos diciendo la teoría del tacataca. Nada, si, si con las fichitas de colores que yo uso, pero yo las uso porque mi mamá era ataura. Entonces tenía fichitas de colores en la casa, esa fue una herencia mía. Pero lo puedo hacer igual con piedras blancas y negras o con monedas de distintas denominaciones o con cualquier cosa con clips desdoblados y doblados, con aretes de mujer de colores diferentes, con botones ¿No? O este, si es entre adultos, con prendas de vestir y te las vas quitando si pierdes. Con lo que se te ocurra.

**octosilabo.com:** Esa es una motivación je, je, je

**Ana Zarina Palafox:** Ya voy a hacer la versión para adultos. Mira el método mío, que es con fichas de póker, pero que puede ser con cualquier otra cosa, monedas, piedras, lo que quieras frijoles ¿No? Semillas diferentes, muñequitos de plástico. La idea es que por un lado es táctil. Tú estás tocando cada frase que dices, tus manos están sintiéndolo.

**octosilabo.com:** Esto de  la plastilina que tu dices. ¿Para qué es? No logré comprender esa herramienta. 

**Ana Zarina Palafox:** Esa, esa es parte de la magia, esa es la multi sensorialidad del asunto. Pero es digo: plastilina, fichas, lápices lo que quieras. Si yo tengo algo en la mano, y a la hora de decirte la frase te lo entrego, estoy tocando mi frase, estás tocando la recepción de la frase. Si yo tengo dos cosas diferentes, y digo una palabra con cada una, o digo un tacataca con cada una en la goma, la velita y te las pongo en la mano te estoy contagiando el tacataca en la goma, la velita. Pero te estoy entregando una palabra que tu tocas y recibes vivencialmente, multi sensorialmente. Por eso es tan fácil te contagien en el Taller. Los trucos de magia son lo táctil y que sea un círculo cerrado. La convivencia en círculo. Entonces estamos haciendo una complicidad. Estamos sumando las neuronas de todos, emocionándonos juntos y usando la multi sensorialidad. Y pa pronto es una cosa que normalmente no está en los cursos de comunicación que dan a los directivos de las empresas, pero tu cuando estás frete a frente, no tedas cuenta de lo que estás oliendo también, no solamente te estás viendo. Entonces esa es la magia...

**octosilabo.com:** Yo creo que hemos todos tenido la experiencia de a través del tacto, transmitir emociones o a través del tacto recibirlas. Cuando la persona pasa la plastilina la una a la otra, espera su turno, y a medida que va viendo de que la plastilina pasa de una mano a la otra va viendo cómo se acerca su turno y va poniendo atención. Cuando r ecibe la plastilina es como un basquetbolista que recibe una pelota y sabe de que tiene que entregarla. Pero para entregarla tiene que hacer algo, hace ese algo y la entrega. O sea, se libera del. . . se libera de lo que tiene que. . .¿Es el mismo mecanismo psicológico o es otro?      

**Ana Zarina Palafox:** Es el mismo y es muchos pues o sea, tu estas recibiendo efectivamente como tu lo pintas, tienes que hacer algo con eso y entonces lo entregas. Curiosamente tu dijiste ¿Qué? Se había liberado eso. Pero ahí estás con un prejuicio, estás pensando que versar duele ¿No? O que el turno duele, porque ya te libras de eso, ya, como, como un examen, ya pasé e examen. No, lo ideal es que desees el turnador ¿No? Desees el objeto, porque ya tienes lo que quieres expresar, no te libras de nada, tienes la oportunidad de expresar y tienes de expresar que es igual a la oportunidad del de junto para expresarla ¿No? Es muchas cosas, es muchas cosas. Pero lo importante, y pedagógicamente es eso, ponerle masadicen. Por eso parece mágico, por eso no me lo creen, pero por eso funciona. 

**octosilabo.com:**  Oye Ana Zarina, aquí viene una pregunta que es una pregunta que, vas a tener que prepararte para contestarla, improvisar, no sé.¿Al final de todo, que crees tu, un poeta o una poeta: nace o se hace?    

**Ana Zarina Palafox:**

Es como un neuronaducto,

es como un neuronaducto,

nadie se crea a si mismo

y es porque hasta el repentismo

es apenas un producto

es muy raro esereducto

hacer siempre los deberes

tanto hombres como mujeres

si probar les da la gana

estimulación temprana

tempranísima si quieres

¿Qué es lo que quiero decir?

Que nadie, nada, nace sabiendo,

y es que como yo lo entiendo

no eres del verso el emir

se carga como un nenir

la tradición ancestral

influencia paternal

o tal vez aprendizaje

el verso que es todo un viaje

no es asunto natural.

Tienes que memorizar

para acercarte al arte

o alguien tuvo que enseñarte

y tuviste que escuchar

para el verso echar a andar

un estímulo requieres

oir tal vez a mujeres

cantando como en la cuna

pero es tener la fortuna

de absorverlo, eso requieres

Sigue octosílabo.com en:

+ Twitter: <https://twitter.com/octosilabos>
+ Correo: <unicornioazul@disroot.com>
+ Web: <https://octosilabo.gitlab.io/>
+ Youtube: <https://www.youtube.com/channel/UCcfzvytq_DgGQBJOA8v-TNw>
+ Feed Podcast: <https://octosilabo.gitlab.io/feed>